package com.vivekvetri.www.batch

import org.apache.spark.SparkContext
import org.apache.spark.SparkContext._
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.SQLContext._ //for implicit DF conversions
import org.apache.spark.SparkConf
import org.apache.spark.sql.functions._

object MeetupBatch {

	def main(args: Array[String]) {

			// reading a chunk of meetup RVSP dataset
			val inputFile = "input/meetup_RVSP.json"

			val conf = new SparkConf().setAppName("Meetup Statistics Application Using Spark SQL and DF")
			val sc = new SparkContext(conf)
			val sqlContext = new SQLContext(sc)

			// Preparing a master dataframe

			val rsvp = sqlContext.read.json(inputFile)
			rsvp.show()
			rsvp.printSchema()

			rsvp.cache()

			// Preparing some useful tables out of the master dataframe

			val venue = rsvp.select("rsvp_id","venue.venue_id","venue.venue_name")
			venue.show()
			//venue.printSchema()

			val event = rsvp.select("rsvp_id","event.event_id","event.event_name","event.time")
			event.show()
			//event.printSchema()
	
			val group = rsvp.select("rsvp_id","group.group_id","group.group_city")
			group.show()
			//event.printSchema()

			val member = rsvp.select("rsvp_id","member")
			member.show()
			//event.printSchema()

			val timesheet = rsvp.select("rsvp_id","mtime")
			timesheet.show()
			//event.printSchema()

			val guestCount = rsvp.select("rsvp_id","guests")
			guestCount.show()
			//event.printSchema()

			// Lets find some heavy invitations
			val eventGuestCount = event.join(guestCount,"rsvp_id")
			eventGuestCount.show()
			println("<<< Heavily invited >>>")
			val heavyInvite = eventGuestCount.filter(eventGuestCount("guests").gt(0))
			heavyInvite.show(3)

			// Lets find some popular venues based on invitation counts in that venue
			val popularVenues = venue.groupBy(venue("venue_name") as "Venue Name").agg(count(venue("venue_name")) as "Count").orderBy(desc("Count"))
			println("<<< Popular Venues >>>")
			popularVenues.show(3)

	}
}
