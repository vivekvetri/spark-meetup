package com.vivekvetri.www.streaming

import org.apache.spark.SparkConf
import org.apache.spark.storage.StorageLevel
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.sql.SQLContext
import org.apache.spark.sql.functions._

/**
 * Usage: MeetupStreaming <hostname> <port>
 * <hostname> and <port> describe the TCP server that Spark Streaming would connect to receive data.
 *
 * To run this on your local machine, you need to first run a Netcat server
 *    `$ curl -D -i http://stream.meetup.com/2/rsvps | nc -lk 9999`
 * and then run the example
 *    `$ spark-submit --class com.vivekvetri.www.streaming.MeetupStreaming --master local[4] 
 *                    target/scala-2.10/meetup-spark-project_2.10-1.0.jar localhost 9999
 */

object MeetupStreaming {
	def main(args: Array[String]) {
		if (args.length < 2) {
			System.err.println("Usage: MeetupStreaming <hostname> <port>")
				System.exit(1)
		}

		// Create the context with a 5 second batch size
		val sparkConf = new SparkConf().setAppName("Meetup Streaming Application")
			val ssc = new StreamingContext(sparkConf, Seconds(5))

			val inputRSVPStream = ssc.socketTextStream(args(0), args(1).toInt, StorageLevel.MEMORY_AND_DISK_SER)

			inputRSVPStream.foreach {
				rdd=>
					val sqlContext = SQLContext.getOrCreate(rdd.sparkContext)
					import sqlContext.implicits._
					val rsvp = sqlContext.read.json(rdd)
					rsvp.cache()
					//rsvp.show()

					val venue = rsvp.select("rsvp_id","venue.venue_id","venue.venue_name")
					//venue.show()

					val event = rsvp.select("rsvp_id","event.event_id","event.event_name","event.time")
					//event.show()

					val guestCount = rsvp.select("rsvp_id","guests")
					//guestCount.show()

					// Lets find some heavy invitations
					val eventGuestCount = event.join(guestCount,"rsvp_id")
					val heavyInvite = eventGuestCount.filter(eventGuestCount("guests").gt(0))
					println("<<< Heavily invited >>>")
					heavyInvite.show(3)

					// Lets find some popular venues based on invitation counts in that venue
					val popularVenues = venue.groupBy(venue("venue_name") as "Venue Name").agg(count(venue("venue_name")) as "Count").orderBy(desc("Count"))
					println("<<< Popular Venues >>>")
					popularVenues.show(3)

			}

		ssc.start()
	  ssc.awaitTermination()
	}
}
