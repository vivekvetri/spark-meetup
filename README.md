# Overview #

This project focuses on demonstration of Spark using "Meetup" dataset. The retrieval is based on simple RESTful HTTP request/response provided by [Meetup API](http://www.meetup.com/meetup_api/) in JSON format. 

Meetup RSVP data sample :

```
{"venue":{"venue_name":"Chehalem Senior Center","lon":-122.97802,"lat":45.324635,"venue_id":16193132},"visibility":"public","response":"yes","guests":0,"member":{"member_id":183129813,"photo":"http:\/\/photos4.meetupstatic.com\/photos\/member\/1\/a\/c\/5\/thumb_242406853.jpeg","member_name":"Mike Z"},"rsvp_id":1607123573,"mtime":1461730381000,"event":{"event_name":"Newberg Steelheaders Meeting Tuesday May 10 at 7:00 pm","event_id":"230672101","time":1462932000000,"event_url":"http:\/\/www.meetup.com\/NW-Steelheading-Group\/events\/230672101\/"},"group":{"group_topics":[{"urlkey":"saltwater-fishing","topic_name":"Saltwater Fishing"},{"urlkey":"anglers","topic_name":"Anglers"},{"urlkey":"fly-fishing","topic_name":"Fly Fishing"},{"urlkey":"sports","topic_name":"Sports and Recreation"},{"urlkey":"fishing","topic_name":"Fishing"},{"urlkey":"salmon-fishing","topic_name":"Salmon Fishing"},{"urlkey":"steelhead-fishing","topic_name":"Steelhead Fishing"},{"urlkey":"outdoors","topic_name":"Outdoors"},{"urlkey":"fishing-techniques","topic_name":"Fishing Techniques"}],"group_city":"Portland","group_country":"us","group_id":1584878,"group_name":"NW Steelhead & Salmon Fishing Group","group_lon":-122.76,"group_urlname":"NW-Steelheading-Group","group_state":"OR","group_lat":45.5}}
```
